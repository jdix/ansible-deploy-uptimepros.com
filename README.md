
# Info

This project is a demonstration written to be a portfolio piece. It uses ansible to deploy a website (stack included) with useful tagging and variable positioning.

&nbsp;
Overview of Steps:

- A virtual machine in Digital Ocean

- Installing nginx

- Installing postgresql

- Configures postgresql

- Configures nginx

- Deploys a GitLab repository (containing a django website, specifically uptimepros.com)

- Configures Let's Encrypt certificate for that website
  
&nbsp;
# Use Guide

There is some setup required due to the nature of what is being accomplished.

### Prior Access Token Generation:

For both GitLab and Django you will need to first set up API access tokens. More explanation TODO.

### Vault Files:
Most projects contain some secret information due to the nature of it being a full stack deployment. The good news is there is a lot of helpful information to build a successful deployment as well. Each vaulted file (should be main.yml for roles with secrets) will include a vault_template.txt in the directory showing which variables must be defined for that role in a vault as well as the vault ID to be used.

### Playbook Variables:
SECRET_KEY: Django Secret Key location. Usually in the settings.py for a project, it was removed and placed in a different file to be able to share the rest of settings without the key itself. Later it is copied by the "deploy code" step to the same directory as the settings.py.
DATABASE_PW: Same description as the "SECRET_KEY"
droplet_name: Name of the droplet to be created to host the website
dns_name: FQDN (minus www, project assumes to create both bare and www name) of the hostname to be used to configure the website. Mostly used in web configuration files and also is used to name the Django database.
django_gunicorn_user: Django gunicorn server process user. The Django app will execute from this users home directory.

### Tags
This playbook leverages several tags for different use cases.

install: Installs postgres and nginx server components.
conf: Configures postgres, nginx, and gunicorn for running a Django application (in a specific directory).
code: Deploys a git project to the server containing all Django code. Also copies any local 'secret files' to the "settings.py" directory (Django Secret Key and Database Password).
ssl: Configures a [Let's Encrypt](https://letsencrypt.org/) free ssl certificate.
destroy: Destroys the Digital Ocean droplet. Useful for debugging.
<!-- dns: -->
&nbsp;
## Initial Deployment
#### First Run
Command:

    ansible-playbook deploy_to_digital_ocean.yml --vault-id digital_ocean_api_token@/path/to/do_api_token_vpw --vault-id gitlab_key@/path/to/gitlab_key_vpw --vault-id postgres_pass@/path/to/postgres_pass_vpw --vault-id conf_pg_4_django@/path/to/conf_pg_4_django_vpw --extra-vars 'droplet_name=digital_ocean_droplet_name' --private-key /path/to/do.key --tags "deploy_vm,install,conf,code"

  

\* Note: First run should not include the ssl tag. You will need to pause to configure public DNS before this will work. It will also need some time to replicate depending on your DNS settings on the digital ocean droplet and your DNS provider. See Issue: https://gitlab.com/jdix/ansible-deploy-uptimepros.com/issues/8
&nbsp;
#### Second Run
The second run is to be run after public DNS is configured to the given droplet IP address.

    ansible-playbook deploy_to_digital_ocean.yml --vault-id digital_ocean_api_token@/path/to/do_api_token_vpw --extra-vars 'droplet_name=digital_ocean_droplet_name' --private-key /path/to/do.key --tags "ssl"

&nbsp;
## Delete Droplet Command
The destroy tag can be used
    ansible-playbook deploy_to_digital_ocean.yml --vault-id digital_ocean_api_token@/path/to/do_api_token_vpw --extra-vars 'droplet_name=digital_ocean_droplet_name' --private-key /path/to/do.key --tags "destroy"
&nbsp;
## Update Code Command

    ansible-playbook deploy_to_digital_ocean.yml --vault-id digital_ocean_api_token@/path/to/do_api_token_vpw --vault-id gitlab_key@/path/to/gitlab_key_vpw --extra-vars 'droplet_name=digital_ocean_droplet_name' --private-key /path/to/do.key --tags "code"
&nbsp;
## Meta Notes

  

Particularly in WSL, while debugging, it might be helpful to also run 'eval $(ssh-agent -s)' and 'ssh-add "path/to/ssh.key"' first. This ensures your keyphrase for your digital ocean droplet remains active between execution attempts. This way you won't need to keep entering the key phrase every ansible run.

  

    eval $(ssh-agent -s)
    ssh-add ~/.ssh/do.key.openssh

&nbsp;


### Credits to:
https://geoffreyhouy.github.io/posts/how-to-set-up-postgresql-for-django-on-windows-10/
&nbsp;
### Check out the project issues for known issues and incoming enhancements
https://gitlab.com/jdix/ansible-deploy-uptimepros.com/issues
<!--
# CREATE DATABASE www_uptimepros;
# CREATE USER www_uptimepros_django WITH PASSWORD 'password';
# # These 3 alter settings seem optional (performance related)
# ALTER ROLE www_uptimepros_django SET client_encoding TO 'utf8';
# ALTER ROLE www_uptimepros_django SET default_transaction_isolation TO 'read committed';
# ALTER ROLE www_uptimepros_django SET timezone TO 'UTC';
# GRANT ALL PRIVILEGES ON DATABASE www_uptimepros TO www_uptimepros_django;
# ALTER USER www_uptimepros_django CREATEDB;
-->